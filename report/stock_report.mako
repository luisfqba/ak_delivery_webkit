## -*- coding: utf-8 -*-
<html>
  <head>
    <style type="text/css">
    ${css}

    body, table {
        font-family: helvetica;
        font-size: 9px;
    }

    .header {
        margin-left: 0px;
        text-align: left;
        font-size: 10px;
    }

    .title {
        font-size: 14px;
        font-weight: bold;
    }

    .list_table {
        text-align: center;
        border-collapse: collapse;
    }

    .list_table td {
        text-align: left;
        font-size: 10px;
    }

    .list_table th {
        font-size: 10px;
    }

    .list_table thead {
        display: table-header-group;
    }

    .address table {
        font-size: 9px;
        border-collapse: collapse;
        margin: 0px;
        padding: 0px;
    }

    .address .invoice {
        margin-top: 10px;
    }

    .address .recipient {
        margin-right: 120px;
        float: right;
    }

    table .address_title {
        font-weight: bold;
    }

    .address td.name {
        font-weight: bold;
    }

    td.amount, th.amount {
        text-align: right;
        white-space: nowrap;
    }

    td.desc, th.desc {
        text-align: left;
    }

    h1 {
        font-size: 13px;
        font-weight: bold;
    }

    tr.line .note {
        font-size: 8px;
    }

    tr.line {
        margin-bottom: 10px;
    }
    /*
     * Mi Css
    */

    .act_as_table {
        display: table;
        width: 100%;
        text-align:center;
        table-layout: fixed;
        page-break-inside: avoid

    }

    .act_as_row  {
        display: table-row;
    }

    .act_as_cell {
        display: table-cell;
        text-align:left;
        font-size:10px;
        padding-right:3px;
        padding-left:3px;
        padding-top:5px;
        padding-bottom:3px;
        clear:both;
    }

    .act_table,
    .act_as_cell,
    .act_as_row {
        word-wrap: break-word;
    }

    .contenedor {
        color: red;
        font-size:100px;
        padding-top:350px;
        -webkit-transform: rotate(-45deg);
    }

    .contenedor_principal {
        position: relative;
        width: 100%;
    }

    .contenedor_contenido {
        position: relative;
        z-index: 2;
    }

    .contenedor_cancelado {
        position: absolute;
        top:0px;
        left:-60px;
        z-index: 1;
    }

    </style>
  </head>
<body>
  <%from datetime import date %>

  <%def name="address(partner)">
    ${partner.street}
    %if hasattr(partner, 'l10n_mx_street3') and partner.l10n_mx_street3:
      ${partner.l10n_mx_street3}
    %endif
    %if hasattr(partner, 'l10n_mx_street4') and partner.l10n_mx_street4:
       ${partner.l10n_mx_street4}
    %endif
    ${partner.street2 or ''},
    ${partner.city or ''}
    <span>C.P. </span>${partner.zip or ''}, ${partner.country_id.name or ''} ${partner.state_id.name or ''}
    </br>
    <span>RFC: ${partner.vat or ''}</span>
  </%def>

  <%def name="name(partner)">
      %if partner.parent_id:
          ${partner.parent_id.name or ''}
          ${partner.title and partner.title.name or ''}
          ${partner.name}
      %else:
          ${partner.title and partner.title.name or ''} ${partner.name}
      %endif
  </%def>



  <div class="act_as_table" style="padding-top:10px;">
      <div class="act_as_row">
          <div class="act_as_cell" style="border:1px solid black; width:20%;">
              <div style="position: absolute; top: 12px; bottom: 0px;">
                      <center>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; ${helper.embed_logo_by_name('logo',90)|n}</center>
              </div>
          </div>
          <div class="act_as_cell" style="border:1px solid black; width:60%">
              <div style="text-align:center; font-size:18px;">
                  <b>MERCANCIA EN TRANSITO</b>
                  <br>
                  Al: ${formatLang(str(date.today()), date=True)}
              </div>
          </div>

      </div>
  </div>

  <%
  def carriage_returns(text):
      return text.replace('\n', '<br />')
  %>

  <!--&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; ${_('Done by')}: ${user.name}-->
  <br/>
  <div style="margin-left:80%; text-align:right">
  </div>
  <div style="text-align:center; font-size:18px; padding-top:10px;">
   <b>Empresa</b>: ${get_partner_name()}
  </div>


  <div class="act_as_table" style="padding-top:10px;">
        <div class="act_as_row" style="">
          <div class="act_as_cell" style="width:15px; text-align:center;">
              <br>
              <b></b>
          </div>
          <div class="act_as_cell" style="border-bottom: 1px solid black; width:55px; text-align:center;">
              <br>
              <b>${_('Producto')}</b>
          </div>
          <div class="act_as_cell" style="border-bottom: 1px solid black; width:10px; text-align:center;">
              <br>
              <b>${_('Solicitados')}</b>
          </div>
          <div class="act_as_cell" style="border-bottom: 1px solid black; width:10px; text-align:center;">
              <br>
              <b>${_('Entregados')}</b>
          </div>
          <div class="act_as_cell" style="border-bottom: 1px solid black; width:10px; text-align:center;">
              <br>
              <b>${_('Por entregar')}</b>
          </div>
        </div>
        <% solicitados = 0.0 %>
        <% entregados = 0.0 %>
        <% entregar = 0.0 %>

        <% sum_solicitados = 0.0 %>
        <% sum_entregados = 0.0 %>
        <% sum_entregar = 0.0 %>

        <!--totales-->
        <% total_solicitados = 0.0 %>
        <% total_entregados = 0.0 %>
        <% total_entregar = 0.0 %>

        <% from collections import defaultdict %>
        <% results, sorted_dates = result() %>
        %for ori,d in sorted_dates.items():
        %for k,v in results.items():
        %if ori == k:
            <!--Row Origen-->
            <div class="act_as_row">
                  <div class="act_as_cell" style="text-align:left;">
                    <b>Fecha:</b> &nbsp; ${get_fecha(k)} </br>
                    <b>Días:</b> &nbsp; ${get_days(k)} <br>
                    <b>Origen:</b> &nbsp;
                       ${k}

                  </div>
                  <div class="act_as_cell" style="border-bottom: 1px solid black; text-align:center; ">

                  </div>
                  <div class="act_as_cell" style="border-bottom: 1px solid black; text-align:center; ">

                  </div>
                  <div class="act_as_cell" style="border-bottom: 1px solid black;  text-align:center; ">

                  </div>
                  <div class="act_as_cell" style="border-bottom: 1px solid black; text-align:center; ">

                  </div>
            </div>

            %for line in v:
                %if line['cant_solicitada']:
                    <% solicitados = line['cant_solicitada'] %>
                %endif
                %if line['cant_entregada']:
                    <% entregados =  line['cant_entregada'] %>
                %endif

                %if line['cant_x_entregar']:
                   <%  entregar =  line['cant_x_entregar'] %>
                %endif
                <div class="act_as_row">
                  <div class="act_as_cell" style="text-align:center; padding:5px;">

                  </div>
                  <div class="act_as_cell" style="border-bottom: 1px solid black; text-align:left; padding:5px;">
                     &nbsp; [${get_product_code(line['product_id']) or ''}] &nbsp;&nbsp;&nbsp;&nbsp${get_product_name(line['product_id']) or ''}
                  </div>
                  <div class="act_as_cell" style="border-bottom: 1px solid black; text-align:center; padding:5px;">
                     ${formatLang(solicitados) or '0.00'}
                  </div>
                  <div class="act_as_cell" style="border-bottom: 1px solid black; text-align:center; padding:5px;">
                     ${formatLang(entregados) or '0.00'}
                  </div>
                  <div class="act_as_cell" style="border-bottom: 1px solid black; text-align:center; padding:5px;">
                     ${formatLang(entregar) or '0.00'}
                  </div>
                </div>


                <% sum_solicitados = sum_solicitados + solicitados %>
                <% sum_entregados = sum_entregados + entregados %>
                <% sum_entregar = sum_entregar + entregar %>
            %endfor
            <!--Row de totales-->
            <div class="act_as_row">
                  <div class="act_as_cell" style="text-align:center; padding:5px;">

                  </div>
                  <div class="act_as_cell" style="text-align:center; padding:5px;">

                  </div>
                  <div class="act_as_cell" style="text-align:center; padding:5px;">
                     <b>${formatLang(sum_solicitados) or '0.00'}</b>
                  </div>
                  <div class="act_as_cell" style="text-align:center; padding:5px;">
                     <b>${formatLang(sum_entregados) or '0.00'}</b>
                  </div>
                  <div class="act_as_cell" style="text-align:center; padding:5px;">
                     <b>${formatLang(sum_entregar) or '0.00'}</b>
                  </div>
            </div>
            <% solicitados = 0.0 %>
            <% entregados = 0.0 %>
            <% entregar = 0.0 %>

            <!--totales-->
            <% total_solicitados = total_solicitados + sum_solicitados %>
            <% total_entregados = total_entregados +  sum_entregados%>
            <% total_entregar = total_entregar + sum_entregar %>


            <% sum_solicitados = 0.0 %>
            <% sum_entregados = 0.0 %>
            <% sum_entregar = 0.0 %>
        %endif
        %endfor
        %endfor
        <div class="act_as_row">
                  <div class="act_as_cell" style="text-align:center; padding:5px;">

                  </div>
                  <div class="act_as_cell" style="text-align:center; padding:5px;">

                  </div>
                  <div class="act_as_cell" style="border-top: 1px solid black; text-align:center; padding:5px;">
                     <b>${formatLang(total_solicitados) or '0.00'}</b>
                  </div>
                  <div class="act_as_cell" style="border-top: 1px solid black; text-align:center; padding:5px;">
                     <b>${formatLang(total_entregados) or '0.00'}</b>
                  </div>
                  <div class="act_as_cell" style="border-top: 1px solid black; text-align:center; padding:5px; text-color: red">
                     <b>${formatLang(total_entregar) or '0.00'}</b>
                  </div>
            </div>
  </div>

  </body>
</html>

# -*- coding: utf-8 -*-
###############################################################################
#
#   Copyright (c) 2011-2013 Camptocamp SA (http://www.camptocamp.com)
#   @author Nicolas Bessi
#   Copyright (c) 2013 Agile Business Group (http://www.agilebg.com)
#   @author Lorenzo Battistini
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################

import operator
from report import report_sxw
from collections import defaultdict
from itertools import groupby
import time
from datetime import datetime
from collections import OrderedDict



class PrintAkDelivery(report_sxw.rml_parse):

    def __init__(self, cursor, uid, name, context):
        super(PrintAkDelivery, self).__init__(cursor, uid, name, context=context)
        self.cursor = self.cr

        self.localcontext.update({
            'time': time.strftime('%d de %B del %Y'),
            'result': self.get_result,
            'get_partner_name': self.get_partner_name,
            'get_product_name': self.get_product_name,
            'get_product_code': self.get_product_code,
            'get_fecha': self.get_fecha,
            'get_days':  self.get_days,
        })

    def get_partner_name(self):
        name = self.datas['form']['partner_id'][1]
        return name

    def get_days(self, origin):
        origin_date = datetime.strptime(self.get_fecha(origin), '%d-%m-%Y')
        days = datetime.today().date() - origin_date.date()
        return days.days


    def get_product_name(self, product_id):

        product_list = self.pool.get('product.product').read(self.cr, self.uid, [product_id], ['default_code','name'], context=None)
        return  product_list[0]['name']

    def get_product_code(self, product_id):
        product_list = self.pool.get('product.product').read(self.cr, self.uid, [product_id], ['default_code'],
                                                             context=None)
        return product_list[0]['default_code']

    def get_fecha(self, origin):
        sql_query = "select date_order from sale_order where name = '%s'" % (origin)
        self.cr.execute(sql_query)
        fecha = self.cr.fetchall()[0][0]
        x= datetime.strptime(fecha, '%Y-%m-%d').strftime('%d-%m-%Y')
        return x

    def get_result(self):
        partner_id = self.datas['form']['partner_id'][0]
        sql_query = """
            select sm.origin, product_id,

            (select sum(product_qty)
            from stock_move smc, stock_picking sp
            where smc.picking_id = sp.id
            and sp.type = 'out'
            and smc.partner_id = %d
            GROUP BY smc.origin,smc.product_id
            HAVING smc.origin=sm.origin and smc.product_id = sm.product_id) as Cant_Solicitada
            ,
            (select sum(product_qty)
            from stock_move smc, stock_picking sp
            where smc.state in ('done','2binvoiced')
            and smc.picking_id = sp.id
            and sp.type = 'out'
            and smc.partner_id = %d
            GROUP BY smc.origin,smc.product_id
            HAVING smc.origin=sm.origin and smc.product_id = sm.product_id) as Cant_Entregada
            ,
            (select sum(product_qty)
            from stock_move smc , stock_picking sp
            where smc.state in ('assigned','waiting','confirmed')
            and smc.picking_id = sp.id
            and sp.type = 'out'
            and smc.partner_id = %d
            GROUP BY smc.origin,smc.product_id
            HAVING smc.origin=sm.origin and smc.product_id = sm.product_id) as Cant_X_Entregar

            from stock_move sm, stock_picking sp
            where sm.state not in ('cancel','done')
            and sm.picking_id = sp.id
            and sp.type = 'out'
            and sm.partner_id = %d
            group by sm.origin, product_id
	    order by sm.origin, product_id
        """ % (partner_id,partner_id,partner_id,partner_id)
        self.cr.execute(sql_query)
        dic_dates = {}
        lines = self.cr.dictfetchall()
        groups = defaultdict(list)
        for line in lines:
            fecha = datetime.strptime(self.get_fecha(line['origin']), '%d-%m-%Y')
            dic_dates[line['origin']] = fecha
            groups[line.get('origin')].append(line)

        sorted_dates = OrderedDict(sorted(dic_dates.items(), key=lambda t: t[1]))
        return groups, sorted_dates

report_sxw.report_sxw('report.webkit.ak_delivery',
                      'stock.move',
                      'addons/ak_delivery_webkit/report/stock_report.mako',
                      parser=PrintAkDelivery)

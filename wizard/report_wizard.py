# -*- encoding: utf-8 -*-
##############################################################################
#
#    Author: Nicolas Bessi, Guewen Baconnier
#    Copyright Camptocamp SA 2011
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

import time

from openerp.osv import orm, osv, fields
from openerp.tools.translate import _

import re
import logging

logger = logging.getLogger(__name__)


class DeliveryReportWebkitWizard(orm.TransientModel):
    _name = "delivery.report.webkit.wizard"
    _columns = {
        'partner_id': fields.many2one(
            'res.partner', 'Empresa',
            required=True),
    }

    def pre_print_report(self, cr, uid, ids, data, context=None):
        res = self.browse(cr, uid, ids, context=context)[0]
        data['form'] = {
            'partner_id': res.partner_id.id,
        }
        
        vals = self.read(cr, uid, ids,
                         ['partner_id'
                          ],
                         context=context)[0]
        data['form'].update(vals)
        return data

    
    def print_report(self, cursor, uid, ids, data, context=None):
        # we update form with display account value
        datas = self.pre_print_report(cursor, uid, ids, data, context=context)
        return {'type': 'ir.actions.report.xml',
                'report_name': 'webkit.ak_delivery',
                'datas': datas}
    
    
    
    